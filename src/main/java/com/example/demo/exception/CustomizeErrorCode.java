package com.example.demo.exception;

public enum CustomizeErrorCode implements ICustomizeErrorCode{
    QUESTION_NOT_FOUND(2001,"╮(・o・)╭寻找的问题不存在或者已被删除~"),
    TARGET_PARAM_NOT_FOUND(2002,"(´Д｀)y-~未选中任何问题或评论进行回复"),
    NO_LOGIN(2003,"(ΘдΘ！)当前操作需要登录，请登录后重试"),
    SYS_ERROR(2004,"(｀Д´*)服务器冒烟了，请等会儿再试哦"),
    TYPE_PARAM_WRONG(2005,"(︶︿︶)＝凸评论类型错误或不存在"),
    COMMENT_NOT_FOUND(2006,"(O_o)?? 评论不存在或者已被删除~"),
    CONTENT_IS_EMPTY(2007,"输入内容不能为空"),
    READ_NOTIFICATION_FAIL(2008,"不能阅读其他用户信息"),
    NOTIFICATION_NOT_FOUND(2009,"消息不存在或已删除")
    ;

    @Override
    public String getMessage(){
        return message;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    private Integer code;
    private String message;

    CustomizeErrorCode(Integer code, String message) {
        this.message = message;
        this.code = code;
    }
}
