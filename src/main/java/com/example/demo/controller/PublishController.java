package com.example.demo.controller;

import com.example.demo.cache.TagsCache;
import com.example.demo.dto.QuestionDTO;
import com.example.demo.model.Question;
import com.example.demo.model.User;
import com.example.demo.service.QuestionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class PublishController {

    @Autowired(required = false)
    private QuestionService questionService;

    @GetMapping("/publish/{id}")
    public String edit(@PathVariable(name = "id") Long id,
                       Model model) {
        QuestionDTO question = questionService.getById(id);
        model.addAttribute("title", question.getTitle());
        model.addAttribute("description", question.getDescription());
        model.addAttribute("tag", question.getTag());
        model.addAttribute("id", question.getId());
        model.addAttribute("tags", TagsCache.get());
        return "publish";
    }

    @GetMapping("/publish")
    public String publish(Model model) {
        model.addAttribute("tags", TagsCache.get());
        return "publish";
    }

    @PostMapping("/publish")
    public String doPublish(
            @RequestParam(value = "title", required = false) String title,
            @RequestParam(value = "description", required = false) String description,
            @RequestParam(value = "tag", required = false) String tag,
            @RequestParam(value = "id", required = false) Long id,
            HttpServletRequest request,
            Model model) {
        //获取提交的内容，方便之后输入有错误时回写到输入框中，节省再输入时间

        model.addAttribute("title", title);
        model.addAttribute("description", description);
        model.addAttribute("tag", tag);
        model.addAttribute("tags", TagsCache.get());

        //从cookies中获取当前登录用户的信息
        User user = (User) request.getSession().getAttribute("user");
        //未获取到，返回错误信息：“用户未登录”
        if (user == null) {
            model.addAttribute("error", "用户未登录");
            return "publish";
        }

        //对用户输入是否输入了内容的判断
        //未输入标题
        if (title == null || title == "") {
            model.addAttribute("error", "标题不能为空");
            return "publish";
        }
        //未输入问题描述
        if (description == null || description == "") {
            model.addAttribute("error", "问题描述不能为空");
            return "publish";
        }
        //未输入至少一个标签
        if (tag == null || tag == "") {
            model.addAttribute("error", "至少添加一个标签 ");
            return "publish";
        }

        String invalid = TagsCache.filterInvalid(tag);
        if(StringUtils.isNotBlank(invalid)){
            model.addAttribute("error", "输入了非法标签 ");
            return "publish";
        }

        //获取到用户信息，将用户输入的问题写入到Question中，并存入数据库
        Question question = new Question();
        question.setTitle(title);
        question.setDescription(description);
        question.setTag(tag);
        question.setCreator(user.getId());
        question.setViewCount(0);
        question.setCommentCount(0);
        question.setLikeCount(0);
        if (id != null) {
            question.setId(id);

        }
        questionService.createOrUpdate(question);

        //完成输入数据库中后，返回到首页，首页应该显示已提交的问题
        return "redirect:/";
    }
}
