package com.example.demo.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PaginationDTO<T> {
    private List<T> data; //分页中储存的数据类型，主要是问题和通知
    private boolean showPrevious; //是否显示向前一页的按钮
    private boolean showFirstPage;  //是否显示返回第一页的按钮
    private boolean showNext;  //是否显示向后一页的按钮
    private boolean showEndPage;  //是否显示返回最后一页的按钮
    private Integer currentPage;  //当前所在的页码数
    private List<Integer> pages = new ArrayList<>(); //页面数列表，储存当前在页面上显示的页面的编号
    private Integer totalPage = 0; //总页面数

    public void setPagination(Integer totalPage, Integer page) {
        //计算应显示的页码总数
        this.totalPage = totalPage;
        this.currentPage = page;
        pages.add(page);
        for (int i = 1; i <= 3; i++) {
            if (page - i > 0) {
                pages.add(0,page - i);
            }
            if (page + i <= totalPage) {
                pages.add(page + i);
            }
        }
        //只有在第一页时没有上一页按钮.
        if (page == 1) {
            showPrevious = false;
        } else {
            showPrevious = true;
        }
        //在最后一页，没有下一页按钮
        if (page == totalPage) {
            showNext = false;
        } else {
            showNext = true;
        }
        //页码数不包含第一页时，显示返回首页按钮。
        if (pages.contains(1)) {
            showFirstPage = false;
        } else {
            showFirstPage = true;
        }
        //页码数不包含最后一页时，显示转到最后一页按钮
        if (pages.contains(totalPage)) {
            showEndPage = false;
        } else {
            showEndPage = true;
        }
    }
}
