package com.example.demo.dto;

import com.example.demo.model.User;
import lombok.Data;
import lombok.ToString;

@Data
public class CommentDTO {
    private Long id;
    private Long parentId;
    private Integer type;
    private Long commentator;
    private Long gmtCreatr;
    private Long gmtModified;
    private Long likeCount;
    private String content;
    private User user;

    public String toString(){
        return id+","+parentId+","+content;
    }
}
